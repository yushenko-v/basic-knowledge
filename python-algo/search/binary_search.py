"""Binary search algorithm."""

# TODO: pip compile or poetry
# pytest
# compare time with python list searching

from typing import List, Optional

from loguru import logger


def binary_search(input_arr: List[int], searched_val: int) -> Optional[int]:
    """
    Binary search algorithm.

    Input data: sorted array (ascending)
    Binary search time complexity: O(log n)
    Brief description:
        Reduce searching segment [start index, end index] twice at every
        iteration based on comparison of middle index array value with
        searched value.

        * in case of
        input_arr[mid_idx] < searched_val
            => guaranteed
        input_arr[mid_idx + 1] <= searched_val =>
        new start searching index = mid_idx + 1 (right side segment reducing)

        * in case of
        input_arr[mid_idx] > searched_val
            => guaranteed
        input_arr[mid_idx - 1] >= searched_val =>
        new end searching index = mid_idx - 1 (left side segment reducing)

    Parameters
    ----------
    input_arr: List[int]
        Input array with ascending based sorted values.
    searched_val: int
        Searched value.

    Returns
    -------
    Optional[int]
        Searched value index in case of value existing else `None`.

    """
    # Check input data.
    if sorted(input_arr) != input_arr:
        raise IOError('`inp_data` is not sorted.')
    if not isinstance(searched_val, int):
        raise IOError('`searched_val` type is expected to be `int`')

    inp_data_size = len(input_arr)

    # Init binary search indexes.
    mid_idx = inp_data_size // 2
    start_idx = 0
    end_idx = inp_data_size - 1

    # Begin binary search algo.
    logger.debug(f'\nStart finding {searched_val} in {input_arr}.')
    iters_num = 0
    while start_idx <= end_idx:
        current_value = input_arr[mid_idx]

        logger.debug(f'Start and end segment indexes: {start_idx}, {end_idx}')
        logger.debug(f'Segment values: '
                     f'[{input_data[start_idx]}, {input_data[end_idx]}]')
        logger.debug(f'Middle index: {mid_idx}')
        logger.debug(f'Current value: {current_value}.')

        if current_value == searched_val:
            return mid_idx
        elif current_value < searched_val:
            start_idx = mid_idx + 1
        elif current_value > searched_val:
            end_idx = mid_idx - 1
        mid_idx = (end_idx + start_idx) // 2
        iters_num += 1
        logger.debug(f'Iterations number: {iters_num}')
    return None
