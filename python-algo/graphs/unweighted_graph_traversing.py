"""
Unweighted graph traversing algorithms (can be modified to vert searching alg)

Algorithms:
* Breadth first traversing using queue
* Depth first traversing using stack
* Depth first traversing using recursion

Notes about graph breads and depth first traversing:
* Work with unweighted graph
* Worst case time complexity: O(|V| + |E|)
* BSF uses queue, DFS uses stack (also can be implemented using recursion)
* DFS can be implemented using recursion, it requires less memory

"""

from collections import namedtuple, deque
from typing import List, Dict, Optional


def traverse_graph_bf(adj_matrix: List[List[bool]],
                      start_vertex_idx: int) -> Dict[int, int]:
    """
    Breadth first graph traversing using queue.

    Time complexity: O(|V|+|E|), where V is the number of vertex,
                                       E is the number of edges.
    Important notes: Work with unweighted graph. Implementation is based on
    queue. Can't be implemented using recursion.
    Brief description: for Vi go through all neighbours and add them to the
    queue. Next step extract the first neighbour of Vi from queue and repeat
    the same.
    For the graph bellow and started point `1` the final result is expected
    to be:
        1----2
        |\  /|\
        | \/ | \
        | /\ |  \
        |/  \|   \     (vertex index, distance from started vertex):
        3----4----5    (1, 0) -> (2, 1) -> (3, 1) -> (4, 1) -> (5, 2)

    Add all neighbours to queue every `for` loop, but filter them when extract
    from queue using `has already visited` filter.

    Parameters
    ----------
    adj_matrix: List[List[bool]]
        Graph adjacency matrix.
    start_vertex_idx: int
        Started vertex.

    Returns
    -------
    visited_vert: Dict[int, int]
        Visited vertices: key - index, value - distance from started vertex.

    """
    Vertex = namedtuple('Vertex', ['vertex_idx', 'distance'])
    queue = deque([Vertex(start_vertex_idx, 0)])
    visited_vert: Dict[int, int] = {}

    while queue:
        cur_vertex = queue.popleft()
        cur_vertex_idx = cur_vertex.vertex_idx
        if cur_vertex_idx not in visited_vert.keys():
            distance = cur_vertex.distance
            for idx, is_connected in enumerate(adj_matrix[cur_vertex_idx]):
                if is_connected:
                    queue.append(Vertex(idx, distance + 1))
            visited_vert.update({cur_vertex_idx: distance})
    return visited_vert


def traverse_graph_bf_modified(adj_matrix: List[List[bool]],
                               start_vertex_idx: int) -> Dict[int, int]:
    """
    Modified breadth first graph traversing using queue.

    Modification proposal.
    What if add vertex to the searching results the first time we have meet it?
    * While loop will reduce because we don't add the same vertex many times.
    * Searched results will contain info about vertices indexes have already
      been in the queue.
    * We will fill the search results on time.

    Parameters
    ----------
    adj_matrix: List[List[bool]]
        Graph adjacency matrix.
    start_vertex_idx: int
        Started vertex.

    Returns
    -------
    found_vert: Dict[int, int]
        Found vertices: key - index, value - distance from started vertex.

    """
    Vertex = namedtuple('Vertex', ['vertex_idx', 'distance'])
    queue = deque([Vertex(start_vertex_idx, 0)])
    found_vert: Dict[int, int] = {start_vertex_idx: 0}
    while queue:
        cur_vertex = queue.popleft()
        new_distance = cur_vertex.distance + 1
        for idx, is_connected in enumerate(adj_matrix[cur_vertex.vertex_idx]):
            if is_connected:
                if idx not in found_vert.keys():
                    found_vert.update({idx: new_distance})
                    queue.append(Vertex(idx, new_distance))
    return found_vert


def traverse_graph_df(adj_matrix: List[List[bool]],
                      start_vertex_idx: int) -> Dict[int, int]:
    """
    Depth first graph traversing using stack.

    Time complexity: O(|V|+|E|), where V is the number of vertex,
                                       E is the number of edges.
    Important notes: Work with unweighted graph. Implementation is based on
    stack (differs from breadth first traversing uses queue).
    Brief description:
        * Start at s. It has distance 0 from itself.
        * Сonsider a node adjacent to s. Call it t. It has distance 1.
          Mark it as visited.
        * Then consider a node adjacent to t that has not yet been visited.
          It has distance 2. Mark it as visited.
        * Repeat until all nodes reachable from s are visited.

    For the graph bellow and started point `1` the final result is expected
    to be (different order is available):
        1----2
        |\  /|\
        | \/ | \
        | /\ |  \
        |/  \|   \     (vertex index, distance from started vertex):
        3----4----5    (1, 0) -> (2, 1) -> (3, 2) -> (4, 3) -> (5, 4)

    Add all neighbours to stack every `for` loop, but filter them when extract
    from stack using `has already visited` filter.

    Can't be modified using the same idea with breadth first tr, but can be
    implemented using recursion.

    Parameters
    ----------
    adj_matrix: List[List[bool]]
        Graph adjacency matrix.
    start_vertex_idx: int
        Started vertex.

    Returns
    -------
    visited_vert: Dict[int, int]
        Visited vertices: key - index, value - distance from started vertex.

    """
    Vertex = namedtuple('Vertex', ['vertex_idx', 'distance'])
    stack = deque([Vertex(start_vertex_idx, 0)])
    visited_vert: Dict[int, int] = {}

    while stack:
        cur_vertex = stack.pop()
        cur_vertex_idx = cur_vertex.vertex_idx
        if cur_vertex_idx not in visited_vert.keys():
            distance = cur_vertex.distance
            for idx, is_connected in enumerate(adj_matrix[cur_vertex_idx]):
                if is_connected:
                    stack.append(Vertex(idx, distance + 1))
            visited_vert.update({cur_vertex_idx: distance})
    return visited_vert


def traverse_graph_df_recursion(adj_matrix: List[List[bool]],
                                start_vertex_idx: int = 0,
                                visited_vert: Optional[Dict[int, int]] = None,
                                distance: int = 0) -> Dict[int, int]:
    """
    Depth first graph traversing using recursion.

    * Add started vertex to visited vert list
    * Go through all neighbours and call recursive traversing started from
      new started vertex - that neighbour
    * Repeat until all neighbours of main started vertex would be visited

    Parameters
    ----------
    adj_matrix: List[List[bool]]
        Graph adjacency matrix.
    start_vertex_idx: int
        Started vertex.
    visited_vert: Optional[Dict[int, int]]
        Visited vertices: key - index, value - distance from started vertex.
    distance: int
        Distance from main started vertex to current.

    Returns
    -------
    visited_vert: Dict[int, int]
        Visited vertices: key - index, value - distance from started vertex.

    """
    if visited_vert is None:
        visited_vert = {}
    visited_vert.update({start_vertex_idx: distance})
    for idx, is_connected in enumerate(adj_matrix[start_vertex_idx]):
        if is_connected and idx not in visited_vert.keys():
            visited_vert = traverse_graph_df_recursion(adj_matrix,
                                                       idx,
                                                       visited_vert,
                                                       distance + 1)
    return visited_vert
