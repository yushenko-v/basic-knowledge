"""Sorting algorithms."""

from typing import List


def selection_sort(data: List[int]) -> List[int]:
    """
    Sort input data array by simple selection.

    Time complexity: O(n^2)
    Brief description: Go through data n := len(data) times and select min
    element each time. n times of n steps (honestly, steps number reduces,
    but we use Big-O Notation) => n * n = O(n^2)

    Parameters
    ----------
    data: List[int]
        Input array.

    Returns
    -------
    sorted_array: List[int]
        Sorted input array.

    """

    def find_smallest(data: List[int]) -> int:
        """
        Find the smallest element index in input array.

        Parameters
        ----------
        data: List[int]
            Input array.

        Returns
        -------
        min_idx: int
            Min element index.

        """
        min_elem = data[0]
        min_idx = 0
        for idx, main_elem in enumerate(data):
            if main_elem < min_elem:
                min_elem = main_elem
                min_idx = idx
        return min_idx

    sorted_data = []

    while len(data) != 0:
        min_idx = find_smallest(data)
        sorted_data.append(data.pop(min_idx))
    return sorted_data


def bubble_sort(data: List[int]):
    """
    Sort input data array using bubble sorting.

    Time complexity: O(n^2)
    Brief description: each step we compare data[step] with data[step + 1]
    and swap if data[step] > data[step + 1].
    Going through all data array elements for the first time we set the max
    element to the correct position.
    Worst case: descending ordered array. Need exactly n := len(data) times
    going through all array elements.

    Parameters
    ----------
    data: List[int]
        Input array.

    Returns
    -------
    data: List[int]
        Sorted input array.

    """
    elem_num = len(data)
    for _ in range(elem_num - 1):
        for i in range(elem_num - 1):
            if data[i] > data[i + 1]:
                data[i], data[i + 1] = data[i + 1], data[i]
    return data


def insertion_sort(data: List[int]):
    """
    Sort input data array using insertion sorting.

    Time complexity: O(n^2)
    Brief description: Go through all array elements. For each element compare
    its value with previous one. If data[step] < data[step - 1] need to swap
    and compare with data[step - 2] ... data[0], else break. Break helps us to
    reduce iterations number. That way at step k we have data[:k - 1] already
    sorted elements.
    Worst case: descending ordered array. Need exactly n := len(data) times
    going through all array elements.

    Parameters
    ----------
    data: List[int]
        Input array.

    Returns
    -------
    data: List[int]
        Sorted input array.

    """
    elem_num = len(data)
    for i in range(1, elem_num):
        idx = i
        for j in range(i - 1, -1, -1):
            if data[idx] < data[j]:
                data[idx], data[j] = data[j], data[idx]
                idx = j
            else:
                break
    return data
